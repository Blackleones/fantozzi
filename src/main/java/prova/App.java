package prova;

import prova.Data.*;
import prova.Managers.*;

public class App 
{
    public static void main( String[] args )
    {
        Database database = new Database();
        UserManager userManager = new UserManager(database);
        FriendManager friendManager = new FriendManager(database);
        SignInManager signInManager = new SignInManager(database, userManager);
        /*
        userManager.Create("leonardo", "aaa");
        userManager.Create("prova", "aaa");

        friendManager.Add("leonardo", "prova");

        System.out.println(friendManager.AreFriends("prova", "leonardo"));
        System.out.println(friendManager.AreFriends("leonardo", "prova"));
        System.out.println(friendManager.AreFriends("leonardo", "unknown"));
        */
    }
}
