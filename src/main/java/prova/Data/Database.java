package prova.Data;

import java.util.ArrayList;
import java.util.List;

import prova.Entities.*;

public class Database {
    public List<User> Users;
    public List<Friend> Friends;
    public List<String> UsersLoggedIn;

    public Database() 
    {
        Users = new ArrayList<User>();
        Friends = new ArrayList<Friend>();
        UsersLoggedIn = new ArrayList<String>();
    }

    public synchronized User GetUserByName(String username) 
    {
        for(int i = 0; i < Users.size(); i++) 
        {
            User u = Users.get(i);

            if(u.Username.equals(username)) 
            {
                return u;
            }
        }

        return null;
    }

    public synchronized void AddUserLoggedIn(String username) 
    {
        UsersLoggedIn.add(username);
    }

    public synchronized void RemoveUserLoggedIn(String username) 
    {
        UsersLoggedIn.remove(username);
    }

    public synchronized void AddUser(User user) 
    {
        Users.add(user);
    }

    public synchronized void AddFriend(Friend friend) 
    {
        Friends.add(friend);
    }

    public synchronized boolean UserIsAlreadyLoggedIn(String username) 
    {
        for(int i = 0; i < UsersLoggedIn.size(); i++) 
        {
            String u = UsersLoggedIn.get(i);

            if(u.equals(username)) 
            {
                return true;
            }
        }

        return false;
    }

    public synchronized boolean AreFriends(String usernameA, String usernameB) 
    {
        for(int i = 0; i < Friends.size(); i++) 
        {
            Friend f = Friends.get(i);

            if(f.UsernameA.equals(usernameA) && f.UsernameB.equals(usernameB)) 
            {
                return true;
            }

            if(f.UsernameA.equals(usernameB) && f.UsernameB.equals(usernameA)) 
            {
                return true;
            }
        }

        return false;
    }
}