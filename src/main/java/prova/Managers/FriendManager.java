package prova.Managers;

import prova.Entities.*;
import prova.Data.*;

public class FriendManager 
{
    private Database _db;

    public FriendManager(Database database) 
    {
        _db = database;
    }

    public Friend Add(String username, String friend)
    {
        Friend newfriend = new Friend(username, friend);
        _db.AddFriend(newfriend);
        return newfriend;
    }

    public boolean AreFriends(String username, String friend) 
    {
        return _db.AreFriends(username, friend);
    }
}
