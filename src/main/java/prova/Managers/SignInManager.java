package prova.Managers;

import prova.Message;
import prova.Data.*;
import prova.Entities.*;

public class SignInManager {
    
    private Database _db;
    private UserManager _userManager;

    public SignInManager(Database database, UserManager userManager) 
    {
        _db = database;
        _userManager = userManager;
    }
    
    public String Login(String username, String password) 
    {
        if(password == null || password.equals("")) 
        {
            return Message.PASSWORD_NOT_DEFINED;
        }

        User user = _userManager.GetUserByName(username);

        if(user == null) 
        {
            return Message.USER_NOT_FOUND;
        }

        if(UserAlreadyLoggedIn(username)) 
        {
            return Message.USER_ALREADY_LOGGED_IN;
        }

        if(user.Password.equals(password)) 
        {
            _db.AddUserLoggedIn(username);
            return Message.SUCCESSED;
        }

        return Message.FAILED;
    }

    private boolean UserAlreadyLoggedIn(String username) 
    {
        return _db.UserIsAlreadyLoggedIn(username);
    }

    public String Logout(String username) 
    {
        if(UserAlreadyLoggedIn(username)) 
        {
            _db.RemoveUserLoggedIn(username);
        }

        return Message.SUCCESSED;
    }
}
