package prova.Managers;

import prova.Data.Database;
import prova.Entities.*;

public class UserManager {

    private Database _db;

    public UserManager(Database database) 
    {
        _db = database;
    }

    public User Create(String username, String password) 
    {
        User user = new User(username, password);
        _db.AddUser(user);
        return user;
    }

    public User GetUserByName(String username) 
    {
        return _db.GetUserByName(username);
    }
}