package prova;

public class Message 
{
    public final static String USER_NOT_FOUND = "USER_NOT_FOUND";
    public final static String USER_ALREADY_LOGGED_IN = "USER_ALREADY_LOGGED_IN";
    public final static String USERNAME_ALREADY_EXISTS = "USERNAME_ALREADY_EXISTS";
    public final static String PASSWORD_NOT_DEFINED = "PASSWORD_NOT_DEFINED";
    public final static String TIMEOUT = "TIMEOUT";

    public final static String SUCCESSED = "SUCCESSED";
    public final static String FAILED = "FAILED";
}